package jp.us717.selenium.common;

public class Define {

    //=======ドライバ===================================================
    /** IEドライバーのパス. */
    public static final String IE_DRIVER = "F:/selenium_project/setting/IEDriverServer.exe";

    //=======ログ=======================================================
    /** ログファイル名 */
    public static final String LOG_FILE_NAME = "";
    /** ログの出力先 */
    public static final String LOG_PATH = "";
}
