package jp.us717.selenium.common;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * ドライバークラス.
 */
public class Driver {

    /** ドライバー. */
    private static WebDriver driver;

    /**
     * コンストラクタ.
     */
    public Driver() {
        init();
    }

    /**
     * ドライバ初期化処理(明示的).
     */
    public void init() {
        System.setProperty("webdriver.ie.driver", Define.IE_DRIVER);
        driver = new InternetExplorerDriver();
    }

    /**
     * ドライバ終了処理(明示的).
     * @throws IOException
     */
    public void quiet() throws IOException {
        driver.quit();
     }

    /**
     * 要素の取得.
     */
    public WebElement findElement(By arg) {
        return driver.findElement(arg);
    }

    /**
     * 待機処理(スレッドスリープ.
     * @param sleepTime :待機時間(ミリ秒)
     * @param logMessage :ログ出力用メッセージ
     */
    public void threadSleep(int sleepTime, String logMessage) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            // ログ出力
        }
    }

    /**
     * 待機処理(スレッドスリープ.
     * @param sleepTime :待機時間(ミリ秒)
     * @throws InterruptedException
     */
    public void threadSleep(int sleepTime) throws InterruptedException {
        Thread.sleep(sleepTime);
    }

    /**
     * キャプチャ取得/
     * @param filepath :出力ディレクト(パス)
     * @param filename :出力ファイル名
     */
    public void capture(String filepath, String filename) {
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File(filepath + "/filename" + ".png"));
        } catch (IOException e) {
            // ログを出力する
        }
    }

    /**
     * ページを開く.
     * @param url :対象URL
     */
    public void openPage(String url) {
        // getリクエスト
        driver.get(url);
    }

    /**
     * ウィンドを閉じる.
     */
    public void close() {
        driver.close();
    }
}
