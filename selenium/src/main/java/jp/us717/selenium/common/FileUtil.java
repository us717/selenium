package jp.us717.selenium.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class FileUtil {

    /**
     * CSVファイルの読み込み(全て)
     * @throws IOException
     */
    public static HashMap<String, String[]> readCSV(String filePath) throws IOException {

        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        HashMap<String,String[]> map = new HashMap<>();

        // 1行づつ読み込む
        String line = br.readLine();
        for(int row=0; line != null; row++) {
            String[] data = line.split(",", 0);
            map.put(String.valueOf(row), data);
        }
        br.close();
        return map;
    }
}
