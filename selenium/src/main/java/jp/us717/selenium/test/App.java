package jp.us717.selenium.test;


import java.io.IOException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.us717.selenium.common.Driver;


/**
 * メインループ
 */
public class App
{
    private final static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) throws IOException {

//        HashMap<String, String[]>hoge = new HashMap<>();
//        hoge =  FileUtil.readCSV("D:/EAS/デスクトップ/test.csv");
//
//        for(int i=0 ; i<hoge.size() ; ++i) {
//            String[] aa = hoge.get(String.valueOf(i));
//            for(int j=0 ; j<aa.length ; ++j) {
//            }
//        }
        logger.info("ああ");

        Driver driver = new Driver();
        // ページを開く
        driver.openPage("http://blog.esuteru.com/");
        driver.capture("D:/EAS/デスクトップ/新しいフォルダ2", "テスト");

        // ドライバ終了処理
        driver.close();
        driver.quiet();
    }
}
